package com.thong.chatmvp.constant

object MsgType {
    const val MYCHAT = 1
    const val OTHERCHAT = 2
}