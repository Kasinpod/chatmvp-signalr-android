package com.thong.chatmvp.repositories.remote.response


import com.google.gson.annotations.SerializedName

data class MessageHistoryRespone(
    @SerializedName("data")
    val data: DataMessageHistory,
    @SerializedName("error_code")
    val errorCode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("sub_code")
    val subCode: Int,
    @SerializedName("title")
    val title: String
)

data class DataMessageHistory (
    @SerializedName("message")
    val message: List<Message>?,
    @SerializedName("total_count")
    val totalCount: Int
)

data class Message(
    @SerializedName("chat_msg_id")
    val chatMsgId: Int,
    @SerializedName("chat_msg_type_id")
    val chatMsgTypeId: Int,
    @SerializedName("chat_msg_type_name")
    val chatMsgTypeName: String,
    @SerializedName("chat_room_id")
    val chatRoomId: Int,
    @SerializedName("create_date")
    val createDate: String,
    @SerializedName("local_msg_id")
    val localMsgId:String,
    @SerializedName("read")
    val read: List<ReadMessage>,
    @SerializedName("text")
    val text: String,
    @SerializedName("user_id")
    val userId: Int
)

data class ReadMessage(
    @SerializedName("object_user_id")
    val objectUserId: String,
    @SerializedName("user_id")
    val userId: Int
)