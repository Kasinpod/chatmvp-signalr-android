package com.thong.chatmvp.utility.extensions

import com.thong.chatmvp.constant.Config
import java.text.SimpleDateFormat
import java.util.*

fun String.getURLHub(
    userId: Int,
    fullName: String,
    firstName: String,
    lastName: String,
    contentId: Int,
    contentPath: String,
    wspId: Int,
    userType: Int,
    UserTypeName: String
): String {
    return "${Config.configUrl.HUB_URL}?UserId=${userId}&FullName=${fullName}&FirstName=${firstName}&LastName=${lastName}&ContentId=${contentId}&ContentPath=${contentPath}&WspId=${wspId}&UserType=${userType}&UserTypeName=${UserTypeName}"
}

fun String.dateFormat(date: Date, outputFormat: String): String {
    val format = SimpleDateFormat(outputFormat)
    return format.format(date)
}