package com.thong.chatmvp.view.home

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.thong.chatmvp.R
import com.thong.chatmvp.repositories.remote.request.GetRoomRequest
import com.thong.chatmvp.repositories.remote.request.UserRoom
import com.thong.chatmvp.repositories.remote.response.GetRoomResponse
import com.thong.chatmvp.repositories.remote.response.UserRespone
import com.thong.chatmvp.utility.SignalRSocket
import com.thong.chatmvp.view.home.adapter.ContactWithSectionAdapter
import com.thong.chatmvp.view.message.MessageActivity
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.json.JSONObject
import org.koin.android.ext.android.inject
import org.zakariya.stickyheaders.StickyHeaderLayoutManager

class HomeFragment : Fragment(), HomeView {

    private val adapterContact = ContactWithSectionAdapter()
    private val presenter = HomePresenter(this)
    private val socket: SignalRSocket by inject()
    private var roomName = ""
    private lateinit var dataModel: UserRespone

    override fun updateUI(`object`: Any) {
        dataModel = `object` as UserRespone
        adapterContact.listSection = presenter.setDate(dataModel)
        adapterContact.notifyAllSectionsDataSetChanged()
    }

    override fun roomResponse(`object`: Any) {
        val model = `object` as GetRoomResponse
        Log.e("model ", model.toString())
        var bundle = Bundle()
        bundle.putSerializable("RoomResponse", model)
        val intent = Intent(context!!, MessageActivity::class.java)
        intent.putExtras(bundle)
        intent.putExtra("RoomName", roomName)
        context!!.startActivity(intent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)


        onInit()

        setRecyclerView(view)

        onEvent()

        return view
    }

    private fun onInit() {
        presenter.getUser()

        socket.getSocketCallback {
            val userId = it.arguments[1].asInt
            if (it.target == socket.EVENT_PUSH_ONLINE){
                checkUserOnline(true,userId)
            }else if (it.target == socket.EVENT_PUSH_OFFLINE){
                checkUserOnline(false,userId)
            }
        }
    }

    private fun checkUserOnline(statusOnline: Boolean, id: Int){
        for (i in dataModel.data?.indices!!) {
            val index = dataModel.data?.get(i)?.userList?.indexOfFirst { it?.userId == id }!!
            adapterContact.listSection[i].items[index].online = statusOnline
            adapterContact.notifySectionItemChanged(i,index)
            break
        }
    }

    private fun setRecyclerView(view: View) {
        view.listContact.apply {
            layoutManager = StickyHeaderLayoutManager()
            adapterContact.listSection = presenter.mockUpData()
            adapter = adapterContact
        }
    }

    private fun onEvent() {
        adapterContact.setOnRecyclerViewClickListener {
            roomName = it.fullName!!
            val list = ArrayList<UserRoom>()
            list.add(UserRoom(it.objectUserId, it.userId))
            presenter.getRoom(
                GetRoomRequest(
                    1,
                    0,
                    "",
                    "Chat Private",
                    list
                )
            )
        }
    }
}