package com.thong.chatmvp.constant

object Config {

    var environment: Environment = Environment.DEV

    var configUrl = when (environment) {
        Environment.DEV -> {
            ConfigUrl(
                BASE_URL = "http://10.0.130.101:8080/dev/micro/chat/v1/",
                PATH_IMAGE = "https://static.uat.afarmmart.com/img/",
                HUB_URL = "http://10.0.130.101:8080/dev/micro/chat/v1/chathub/",
                IDENTITY_URL = "http://10.0.130.101:8080/dev/micro/identity/v1/"
            )
        }
        Environment.UAT -> {
            ConfigUrl(
                BASE_URL = "",
                PATH_IMAGE = "",
                HUB_URL = "",
                IDENTITY_URL = ""
            )
        }
        Environment.PRO -> {
            ConfigUrl(
                BASE_URL = "",
                PATH_IMAGE = "",
                HUB_URL = "",
                IDENTITY_URL = ""
            )
        }
    }
}

data class ConfigUrl(
    val BASE_URL: String,
    val PATH_IMAGE: String,
    val HUB_URL: String,
    val IDENTITY_URL : String
)

enum class Environment {
    DEV, UAT, PRO
}