package com.thong.chatmvp.view.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.thong.chatmvp.R
import com.thong.chatmvp.utility.MySharedPreference
import com.thong.chatmvp.utility.SignalRSocket
import com.thong.chatmvp.utility.extensions.getURLHub
import com.thong.chatmvp.view.home.HomeFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {

    private val socket: SignalRSocket by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadFragment(HomeFragment())

        configSignalR()

        onEvent()
    }

    private fun configSignalR() {
        socket.apply {
            init(
                MySharedPreference.getToken()!!,
                String().getURLHub(
                    MySharedPreference.getUserID(),
                    MySharedPreference.getFullName()!!,
                    MySharedPreference.getFirstName()!!,
                    MySharedPreference.getLastName()!!,
                    MySharedPreference.getContentId(),
                    MySharedPreference.getContentPath()!!,
                    MySharedPreference.getWSPID(),
                    MySharedPreference.getUserType(),
                    MySharedPreference.getUserTypeName()!!
                )
            )
            getConnection(this@MainActivity)
        }
    }

    private fun onEvent() {
        bottom_navigation_view_linear.setNavigationChangeListener { view, _ ->

            when (view?.id) {
                R.id.l_item_home -> {
                    loadFragment(HomeFragment())
                }

                R.id.l_item_list -> {

                }
            }
        }

    }

    private fun loadFragment(fragment: Fragment?) {
        if (fragment != null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.frame, fragment)
                .commit()

        }
    }

    override fun onDestroy() {
        super.onDestroy()
        socket.onDisConnection()
    }
}

