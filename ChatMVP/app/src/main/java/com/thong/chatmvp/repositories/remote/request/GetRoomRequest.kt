package com.thong.chatmvp.repositories.remote.request

import com.google.gson.annotations.SerializedName

data class GetRoomRequest(
    @SerializedName("chat_type_id")
    val chatTypeId: Int?,
    @SerializedName("chat_content_id")
    val chatContentId: Int?,
    @SerializedName("chat_content_path")
    val chatContentPath: String?,
    @SerializedName("chat_room_name")
    val chatRoomName: String?,
    @SerializedName("user")
    val user: List<UserRoom?>?
)

data class UserRoom(
    @SerializedName("object_user_id")
    val objectUserId: String?,
    @SerializedName("user_id")
    val userId: Int?
)