package com.thong.chatmvp.utility

import android.content.Context
import android.content.SharedPreferences

object MySharedPreference {

    private val PREF_NAME = "myapp"
    private val PREF_TOKEN = "token"
    private val PREF_CHACK_LOGIN = "chack"
    private val PREF_OBJECT_USER_ID = "object_user_id"
    private val PREF_USER_ID = "user_id"
    private val PREF_WSP_ID = "wsp_id"
    private val PREF_CONTENT_ID = "content_id"
    private val PREF_CONTENT_PATH = "content_path"
    private val PREF_FIRST_NAME = "first_name"
    private val PREF_FULL_NAME = "full_name"
    private val PREF_LAST_NAME = "last_name"
    private val PREF_USER_TYPE = "user_type"
    private val PREF_USER_TYPE_NAME = "user_type_name"


    private lateinit var sharedPreferences: SharedPreferences

    fun init(context: Context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0)
    }

    private fun getPrefs(): SharedPreferences {
        return sharedPreferences
    }

    fun getToken(): String? {
        return getPrefs()
            .getString(PREF_TOKEN, null)
    }

    fun setToken(data: String?) {
        getPrefs().edit().putString(PREF_TOKEN, data).commit()
    }

    fun getChackLogin(): Boolean {
        return getPrefs().getBoolean(PREF_CHACK_LOGIN, false)
    }

    fun setChackLogin(chack: Boolean) {
        getPrefs().edit().putBoolean(PREF_CHACK_LOGIN, chack).commit()
    }

    fun getObjectID() : String? {
        return getPrefs().getString(PREF_OBJECT_USER_ID, "")
    }

    fun setObjectID(object_user_id : String?){
        getPrefs().edit().putString(PREF_OBJECT_USER_ID, object_user_id).commit()
    }

    fun getUserID() : Int {
        return getPrefs().getInt(PREF_USER_ID, -1)
    }

    fun setUserID(user_id : Int) {
        getPrefs().edit().putInt(PREF_USER_ID, user_id).commit()
    }

    fun getWSPID() : Int {
        return  getPrefs().getInt(PREF_WSP_ID, -1)
    }
    fun setWSPID(wsp_id : Int) {
        getPrefs().edit().putInt(PREF_WSP_ID, wsp_id).commit()
    }

    fun getContentId() : Int {
        return  getPrefs().getInt(PREF_CONTENT_ID, -1)
    }
    fun setContentId(content_id : Int) {
        getPrefs().edit().putInt(PREF_CONTENT_ID, content_id).commit()
    }

    fun getContentPath() : String? {
        return  getPrefs().getString(PREF_CONTENT_PATH, "")
    }
    fun setContentPath(content_path : String? ) {
        getPrefs().edit().putString(PREF_CONTENT_PATH, content_path).commit()
    }

    fun getFirstName() : String? {
        return  getPrefs().getString(PREF_FIRST_NAME, "")
    }
    fun setFirstName(first_name : String? ) {
        getPrefs().edit().putString(PREF_FIRST_NAME, first_name).commit()
    }

    fun getFullName() : String? {
        return  getPrefs().getString(PREF_FULL_NAME, "")
    }
    fun setFullName(full_name : String? ) {
        getPrefs().edit().putString(PREF_FULL_NAME, full_name).commit()
    }

    fun getLastName() : String? {
        return  getPrefs().getString(PREF_LAST_NAME, "")
    }
    fun setLastName(last_name : String? ) {
        getPrefs().edit().putString(PREF_LAST_NAME, last_name).commit()
    }

    fun getUserType() : Int {
        return  getPrefs().getInt(PREF_USER_TYPE, -1)
    }
    fun setUserType(user_type : Int) {
        getPrefs().edit().putInt(PREF_USER_TYPE, user_type).commit()
    }

    fun getUserTypeName() : String? {
        return  getPrefs().getString(PREF_USER_TYPE_NAME, "")
    }
    fun setUserTypeName(user_type_name : String? ) {
        getPrefs().edit().putString(PREF_USER_TYPE_NAME, user_type_name).commit()
    }
}