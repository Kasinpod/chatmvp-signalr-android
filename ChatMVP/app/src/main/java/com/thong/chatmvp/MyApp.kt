package com.thong.chatmvp

import android.app.Application
import com.thong.chatmvp.di.appModule
import com.thong.chatmvp.utility.MySharedPreference
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class MyApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidLogger()
            androidContext(this@MyApp)
            modules(listOf(appModule))
        }
        MySharedPreference.init(this)
    }
}