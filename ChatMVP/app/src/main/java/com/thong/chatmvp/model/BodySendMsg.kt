package com.thong.chatmvp.model

data class BodySendMsg (
    val room_id: Int?,
    val message: String?,
    val local_msg_id: String?
)