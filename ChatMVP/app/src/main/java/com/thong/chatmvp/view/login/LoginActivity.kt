package com.thong.chatmvp.view.login

import android.content.Intent
import android.os.Bundle
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.thong.chatmvp.R
import com.thong.chatmvp.repositories.remote.request.LoginRequest
import com.thong.chatmvp.repositories.remote.response.LoginRespone
import com.thong.chatmvp.utility.CryptoUtility
import com.thong.chatmvp.utility.MySharedPreference
import com.thong.chatmvp.view.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginView {

    private val loginPresenter = LoginPresenter(this)

    override fun updateUI(`object`: Any) {
        val loginResponse = `object` as LoginRespone

        MySharedPreference.setToken(loginResponse.accessToken)
        MySharedPreference.setChackLogin(true)
        MySharedPreference.setObjectID(loginResponse.objectUserId)

        MySharedPreference.setUserID(loginResponse.userId!!)

        MySharedPreference.setWSPID(loginResponse.wspId)
        //
        MySharedPreference.setFullName(subString(loginResponse.fullName))
        MySharedPreference.setFirstName(loginResponse.firstName)
        MySharedPreference.setLastName(loginResponse.lastName)
        MySharedPreference.setContentId(loginResponse.contentId!!)
        MySharedPreference.setContentPath(loginResponse.contentPath)
        MySharedPreference.setWSPID(loginResponse.wspId)
        MySharedPreference.setUserType(loginResponse.userType)
        MySharedPreference.setUserTypeName(subString(loginResponse.userTypeName))

        var intent = Intent(this@LoginActivity, MainActivity::class.java)
        startActivity(intent)
        finish()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        onEvent()
    }

    private fun onEvent() {
        buttonLogin.setOnClickListener {
            if (validateInput()) {
                callApiLogin(
                    editTextUserName.text.toString().trim(),
                    editTextPassword.text.toString().trim()
                )
            }
        }
    }

    private fun callApiLogin(userName: String, password: String) {
        val loginModel = LoginRequest(
            "password",
            userName,
            CryptoUtility.toHMACSHA384("o2o.platform", password),
            "APIIdentity APIChat offline_access",
            "Client.o2o.platform",
            "SgDhDx1IOZ7rvM2do43FD21CRAT35oTW",
            "username",
            "1",
            "99"
        )
        loginPresenter.callApiLogin(loginModel)
    }

    private fun validateInput(): Boolean {
        return when {
            editTextUserName.text.toString().trim().isEmpty() -> {
                editTextUserName.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextUserName.requestFocus()
                false
            }
            editTextPassword.text.toString().trim().isEmpty() -> {
                editTextPassword.startAnimation(
                    AnimationUtils.loadAnimation(
                        this,
                        R.anim.shaking_view
                    )
                )
                editTextPassword.requestFocus()
                false
            }
            else -> true
        }
    }

    private fun subString(string: String) : String {
        var t = ""
        for (i in string){
            if (!i.toString().contains(" ")) {
                t+=i
            }
        }
        return t
    }
}
