package com.thong.chatmvp.view.message

import android.os.Bundle
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.thong.chatmvp.R
import com.thong.chatmvp.model.BodySendMsg
import com.thong.chatmvp.repositories.cache.AppDatabase
import com.thong.chatmvp.repositories.cache.entity.MessageEntity
import com.thong.chatmvp.repositories.remote.response.GetRoomResponse
import com.thong.chatmvp.repositories.remote.response.MessageHistoryRespone
import com.thong.chatmvp.model.DataChat
import com.thong.chatmvp.utility.MySharedPreference
import com.thong.chatmvp.utility.SignalRSocket
import com.thong.chatmvp.utility.extensions.hideKeyboard
import com.thong.chatmvp.utility.extensions.isOnline
import com.thong.chatmvp.view.message.adapter.ChatAdapter
import kotlinx.android.synthetic.main.activity_message.*
import org.json.JSONObject
import org.koin.android.ext.android.inject
import java.util.*
import kotlin.collections.ArrayList


class MessageActivity : AppCompatActivity(), MessageView {

    private lateinit var modelRoom: GetRoomResponse
    private val appDatabase: AppDatabase by inject()
    private val socket: SignalRSocket by inject()
    private val presenter = MessagePresenter(this, appDatabase)
    private val chatAdapter = ChatAdapter()
    private var listDataMsg: ArrayList<MessageEntity> = arrayListOf()

    override fun updateUI(`object`: Any) {
        listDataMsg.clear()
        listDataMsg = `object` as ArrayList<MessageEntity>
        chatAdapter.itemList = listDataMsg
        chatAdapter.notifyDataSetChanged()
        listMsg.scrollToPosition(listDataMsg.size - 1)
    }

    override fun responseHistory(`object`: Any) {
        val model = `object` as MessageHistoryRespone
        var msgData: ArrayList<MessageEntity> = ArrayList()
        model.data.message?.forEach {
            msgData.add(
                MessageEntity(
                    localMsgId = it.localMsgId,
                    chatMsgId = it.chatMsgId,
                    chatMsgTypeId = it.chatMsgTypeId,
                    chatRoomId = it.chatRoomId,
                    text = it.text,
                    userId = it.userId,
                    send_msg_status = 2,
                    massage_time = it.createDate
                )
            )
        }
        Log.e("chatRoomId : ", modelRoom.data?.chatRoomId.toString())
        if (msgData.size != 0) presenter.insertAllMsg(msgData,modelRoom.data?.chatRoomId!!)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_message)

        onInit()
        onReadMessage()
        onEvent()

    }

    private fun onInit() {
        val bundle = intent.extras
        val buttonBack = toolbar_mess.findViewById<ImageView>(R.id.toolbarbuttonBack)
        val title = toolbar_mess.findViewById<TextView>(R.id.toolbartextViewTitle)
        modelRoom = bundle?.getSerializable("RoomResponse") as GetRoomResponse
        title.text = intent.getStringExtra("RoomName")
        buttonBack.setOnClickListener {
            hideKeyboard()
            finish()
        }
        presenter.getHistoryMessage(modelRoom.data?.chatRoomId!!, 1)
        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        listMsg.apply {
            layoutManager = LinearLayoutManager(this@MessageActivity, RecyclerView.VERTICAL, false)
            adapter = chatAdapter
        }

        listMsg.addOnLayoutChangeListener { view, left, top, right, bottom, oldLeft, oldTop, oldRight, oldBottom ->
            if (bottom < oldBottom) {
                listMsg.postDelayed(
                    {
                        listMsg.scrollToPosition(listDataMsg.size - 1)
                    },
                    100
                )
            }
        }
    }

    private fun onEvent() {
        imageView.setOnClickListener {
            if (editText.text.toString().trim().isNotEmpty()) {
                onSendMessage(editText.text.toString())
                editText.setText("")
            } else {
                editText.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shaking_view))
            }
        }
    }

    private fun onSendMessage(message: String) {
        val localMsgId: String = UUID.randomUUID().toString()
        val body = BodySendMsg(modelRoom.data?.chatRoomId, message, localMsgId)

        val model = MessageEntity(
            localMsgId = localMsgId,
            chatMsgId = 0,
            chatMsgTypeId = 0,
            chatRoomId = modelRoom.data?.chatRoomId,
            text = message,
            userId = MySharedPreference.getUserID(),
            send_msg_status = 0,
            massage_time = ""
        )
        presenter.insertMsg(model)

        if (isOnline()){
            socket.sendMessage(body)
        }
    }

    private fun onReadMessage() {
        socket.getSocketCallback {
            if (it.target == socket.EVENT_PUSH) {
                val jsonObject = it.arguments[0].asJsonObject
                val model = Gson().fromJson(jsonObject, DataChat::class.java)
                println("onReadMessage : $model")

                if (model.userId == MySharedPreference.getUserID()) {
                    presenter.updateStatusSendMsgAndTimeStamp(model)
                } else {
                    val data = MessageEntity(
                        localMsgId = model.localMsgId,
                        chatMsgId = model.chatMsgId,
                        chatMsgTypeId = model.chatMsgTypeId,
                        chatRoomId = model.chatRoomId,
                        text = model.text,
                        userId = model.userId,
                        send_msg_status = 2,
                        massage_time = model.dateTime
                    )
                    presenter.insertMsg(data)
                }
            }
        }
    }
}
