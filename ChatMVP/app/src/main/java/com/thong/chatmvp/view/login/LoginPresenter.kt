package com.thong.chatmvp.view.login

import com.thong.chatmvp.constant.Config
import com.thong.chatmvp.repositories.remote.CallApi
import com.thong.chatmvp.repositories.remote.request.LoginRequest
import com.thong.chatmvp.repositories.remote.response.LoginRespone
import com.thong.chatmvp.utility.RetrofitBuilder
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class LoginPresenter(private val view: LoginView) {

    var retrofit = RetrofitBuilder().build<CallApi>(Config.configUrl.IDENTITY_URL)

    fun callApiLogin(model: LoginRequest) {
        retrofit.logIn(
            model.grantType,
            model.username,
            model.password,
            model.scope,
            model.clientId,
            model.clientSecret,
            model.authenType,
            model.wspId,
            model.sysId
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LoginRespone> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(t: LoginRespone) {
                    view.updateUI(t)
                }

                override fun onError(e: Throwable) {

                }

            })
    }

}
