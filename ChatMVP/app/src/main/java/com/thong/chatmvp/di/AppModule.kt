package com.thong.chatmvp.di

import androidx.room.Room
import com.thong.chatmvp.repositories.cache.AppDatabase
import com.thong.chatmvp.utility.SignalRSocket
import org.koin.dsl.module

val appModule = module {

    single { Room.databaseBuilder(get(), AppDatabase::class.java, "chat_database").build() }
    single { get<AppDatabase>().HistoryMessageDao() }

    single { SignalRSocket() }
}