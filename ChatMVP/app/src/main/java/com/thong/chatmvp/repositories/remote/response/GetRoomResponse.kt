package com.thong.chatmvp.repositories.remote.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GetRoomResponse(
    @SerializedName("data")
    val `data`: DataRoom?,
    @SerializedName("error_code")
    val errorCode: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("sub_code")
    val subCode: Int?,
    @SerializedName("title")
    val title: String?
) : Serializable

data class DataRoom(
    @SerializedName("chat_content_id")
    val chatContentId: Int?,
    @SerializedName("chat_content_path")
    val chatContentPath: String?,
    @SerializedName("chat_room_id")
    val chatRoomId: Int?,
    @SerializedName("chat_room_name")
    val chatRoomName: String?,
    @SerializedName("chat_type_id")
    val chatTypeId: Int?,
    @SerializedName("chat_type_name")
    val chatTypeName: String?,
    @SerializedName("create_by")
    val createBy: Int?,
    @SerializedName("create_date")
    val createDate: String?,
    @SerializedName("edit_by")
    val editBy: Int?,
    @SerializedName("edit_date")
    val editDate: String?,
    @SerializedName("inactive")
    val inactive: Boolean?,
    @SerializedName("message_last")
    val messageLast: Any?,
    @SerializedName("noti_number")
    val notiNumber: Int?,
    @SerializedName("user")
    val user: List<ListUserRoom?>?,
    @SerializedName("wsp_id")
    val wspId: Int?
): Serializable

data class ListUserRoom(
    @SerializedName("content_id")
    val contentId: Int?,
    @SerializedName("content_path")
    val contentPath: String?,
    @SerializedName("first_name")
    val firstName: String?,
    @SerializedName("full_name")
    val fullName: String?,
    @SerializedName("last_name")
    val lastName: String?,
    @SerializedName("object_user_id")
    val objectUserId: String?,
    @SerializedName("user_id")
    val userId: Int?,
    @SerializedName("user_type")
    val userType: Int?,
    @SerializedName("user_type_name")
    val userTypeName: String?
): Serializable