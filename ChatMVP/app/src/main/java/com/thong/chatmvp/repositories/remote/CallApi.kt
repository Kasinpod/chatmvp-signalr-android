package com.thong.chatmvp.repositories.remote

import com.thong.chatmvp.repositories.remote.request.GetRoomRequest
import com.thong.chatmvp.repositories.remote.response.GetRoomResponse
import com.thong.chatmvp.repositories.remote.response.LoginRespone
import com.thong.chatmvp.repositories.remote.response.MessageHistoryRespone
import com.thong.chatmvp.repositories.remote.response.UserRespone
import io.reactivex.Observable
import retrofit2.http.*

interface CallApi {

    @FormUrlEncoded
    @POST("connect/token")
    fun logIn(
        @Field("grant_type") grant_type: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("scope") scope: String,
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String,
        @Field("authen_type") authen_type: String,
        @Field("wsp_id") wsp_id: String,
        @Field("sys_id") sys_id: String
    ): Observable<LoginRespone>

    @GET("chat/contact/list")
    fun getUserList(
        @Header("Authorization") token: String,
        @Query("wsp_id") wsp_id: Int,
        @Query("user_type") user_type: Int
    ):Observable<UserRespone>

    @POST("chat/room")
    fun getRoom(
        @Header("Authorization") token: String,
        @Query("object_user_id") object_user_id: String,
        @Query("user_id") user_id: Int,
        @Query("wsp_id") wsp_id: Int,
        @Body getRoomRequest: GetRoomRequest
    ):Observable<GetRoomResponse>

    @GET("chat/room/history")
    fun getMessageHistory(
        @Header("Authorization") token: String,
        @Query("chat_msg_id") chat_msg_id: Int,
        @Query("chat_room_id") chat_room_id: Int,
        @Query("user_id") user_id: Int,
        @Query("wsp_id") wsp_id: Int,
        @Query("page_size") page_size: Int,
        @Query("current_page") current_page: Int
    ):Observable<MessageHistoryRespone>
}