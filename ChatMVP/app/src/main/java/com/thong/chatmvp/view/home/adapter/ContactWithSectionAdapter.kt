package com.thong.chatmvp.view.home.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.thong.chatmvp.R
import com.thong.chatmvp.repositories.remote.response.User
import com.thong.chatmvp.model.SectionItem
import de.hdodenhof.circleimageview.CircleImageView
import org.zakariya.stickyheaders.SectioningAdapter

class ContactWithSectionAdapter : SectioningAdapter() {

    var listSection = arrayListOf<SectionItem>()
    private lateinit var mCallback: (User) -> Unit

    fun setOnRecyclerViewClickListener(callback: (User) -> Unit) {
        this.mCallback = callback
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup, headerType: Int): HeaderViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v: View =
            inflater.inflate(R.layout.item_header_contact, parent, false)
        return HeaderViewHolder(v)

    }

    override fun onCreateItemViewHolder(parent: ViewGroup, itemType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v: View =
            inflater.inflate(R.layout.item_row_contact, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindHeaderViewHolder(
        viewHolder: SectioningAdapter.HeaderViewHolder,
        sectionIndex: Int,
        headerType: Int
    ) {
        val holder = viewHolder as HeaderViewHolder
        var section = listSection[sectionIndex]

        holder.text_header.text = section.title

        holder.imageView.setOnClickListener {
            setSectionIsCollapsed(sectionIndex, !isSectionCollapsed(sectionIndex))
            if (!isSectionCollapsed(sectionIndex)) {
                holder.imageView.rotation = 180F
            } else {
                holder.imageView.rotation = 0F
            }
        }

    }

    @SuppressLint("ResourceAsColor")
    override fun onBindItemViewHolder(
        viewHolder: SectioningAdapter.ItemViewHolder,
        sectionIndex: Int,
        itemIndex: Int,
        itemType: Int
    ) {
        val holder = viewHolder as ItemViewHolder
        val item = listSection[sectionIndex].items[itemIndex]

        holder.user_item.text = item.fullName

        if (item.online!!) {
            holder.messagr_item.setTextColor(R.color.colorAccent)
            holder.messagr_item.text = "ออนไลน์"
        } else {
            holder.messagr_item.setTextColor(R.color.click_inactive)
            holder.messagr_item.text = "ออฟไลน์"
        }

        holder.itemView.setOnClickListener {
            mCallback.invoke(item)
        }

    }

    inner class HeaderViewHolder(itemView: View) :
        SectioningAdapter.HeaderViewHolder(itemView) {

        var imageView = itemView.findViewById<ImageView>(R.id.imageView_down)
        var text_header = itemView.findViewById<TextView>(R.id.text_header)

    }

    inner class ItemViewHolder(itemView: View) :
        SectioningAdapter.ItemViewHolder(itemView) {

        var image_user = itemView.findViewById<CircleImageView>(R.id.image_user)
        var user_item = itemView.findViewById<TextView>(R.id.user_item)
        var messagr_item = itemView.findViewById<TextView>(R.id.messagr_item)


    }

    override fun getNumberOfSections(): Int {
        return listSection.size
    }

    override fun getNumberOfItemsInSection(sectionIndex: Int): Int {
        return listSection[sectionIndex].items.size
    }

    override fun doesSectionHaveHeader(sectionIndex: Int): Boolean {
        return true
    }

    override fun doesSectionHaveFooter(sectionIndex: Int): Boolean {
        return false
    }
}
