package com.thong.chatmvp.repositories.remote.response

import com.google.gson.annotations.SerializedName

data class LoginRespone(
    @SerializedName("access_token")
    val accessToken: String,
    @SerializedName("ContentId")
    val contentId: Int?,
    @SerializedName("ContentPath")
    val contentPath: String,
    @SerializedName("ExpDate")
    val expDate: String,
    @SerializedName("expires_in")
    val expiresIn: Int?,
    @SerializedName("FirstName")
    val firstName: String,
    @SerializedName("FullName")
    val fullName: String,
    @SerializedName("LastName")
    val lastName: String,
    @SerializedName("ObjectUserId")
    val objectUserId: String,
    @SerializedName("refresh_token")
    val refreshToken: String,
    @SerializedName("scope")
    val scope: String,
    @SerializedName("token_type")
    val tokenType: String,
    @SerializedName("UserId")
    val userId: Int?,
    @SerializedName("UserType")
    val userType: Int,
    @SerializedName("UserTypeName")
    val userTypeName: String,
    @SerializedName("WspId")
    val wspId: Int
)