package com.thong.chatmvp.model

import com.thong.chatmvp.repositories.remote.response.User

data class SectionItem (
    var title:String = "",
    var items:ArrayList<User> = ArrayList()
)