package com.thong.chatmvp.view.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.thong.chatmvp.R
import com.thong.chatmvp.constant.Config
import com.thong.chatmvp.constant.Environment
import com.thong.chatmvp.utility.MySharedPreference
import com.thong.chatmvp.view.login.LoginActivity
import com.thong.chatmvp.view.main.MainActivity

class SplashScreenActivity : AppCompatActivity() {

    private var handle: Handler? = null
    private var runnable: Runnable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spash)

        //Config
        Config.environment = Environment.DEV

        handle = Handler()

        if (MySharedPreference.getChackLogin()){
            startMain()
        }else{
            startLogin()
        }

    }

    private fun startMain() {
        runnable = Runnable {
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun startLogin() {
        runnable = Runnable {
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        handle!!.postDelayed(runnable, 3000)
    }

    override fun onStop() {
        super.onStop()
        handle!!.removeCallbacks(runnable)
    }

}
