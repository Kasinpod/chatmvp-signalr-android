package com.thong.chatmvp.utility

import android.app.Activity
import android.widget.Toast
import com.google.gson.Gson
import com.smartarmenia.dotnetcoresignalrclientjava.*
import com.thong.chatmvp.model.BodySendMsg
import org.json.JSONObject
import java.lang.Exception

class SignalRSocket : HubConnectionListener, HubEventListener {

    private lateinit var connection: HubConnection
    lateinit var mCallback: (HubMessage) -> Unit
    lateinit var mOnlineCallback: (Boolean, Int) -> Unit

    private val EVENT_SEND = "SendMessage"
    val EVENT_PUSH = "PushMessage"
    val EVENT_PUSH_ONLINE = "PushOnline"
    val EVENT_PUSH_OFFLINE = "PushOffLine"

    fun init(token: String, hubUrl: String) {
        println("token : $token")
        println("hubUrl : $hubUrl")
        connection = WebSocketHubConnectionP2(
            hubUrl,
            "Bearer $token"
        )
        connection.addListener(this)
        connection.subscribeToEvent(EVENT_PUSH, this)
        connection.subscribeToEvent(EVENT_PUSH_ONLINE, this)
        connection.subscribeToEvent(EVENT_PUSH_OFFLINE, this)
    }

    fun getSocketCallback(callback: (HubMessage) -> Unit) {
        this.mCallback = callback
    }

    fun getOnlineCallback(callback: (Boolean, Int) -> Unit) {
        this.mOnlineCallback = callback
    }

    fun getConnection(activity: Activity) {
        try {
            connection.connect()
        } catch (ex: Exception) {
            activity.runOnUiThread {
                Toast.makeText(activity, ex.message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun onDisConnection() {
        connection.removeListener(this)
        connection.unSubscribeFromEvent(EVENT_PUSH, this)
        connection.unSubscribeFromEvent(EVENT_PUSH_ONLINE, this)
        connection.unSubscribeFromEvent(EVENT_PUSH_OFFLINE, this)
        connection.disconnect()
    }

    fun sendMessage(body: BodySendMsg) {
        connection.invoke(EVENT_SEND, body)
    }

    override fun onConnected() {
        println("onConnected : Connected")
    }

    override fun onMessage(message: HubMessage?) {

    }

    override fun onDisconnected() {

    }

    override fun onError(exception: Exception?) {
        println(
            "message onError : ${exception?.message}"
        )
    }

    override fun onEventMessage(message: HubMessage?) {
        mCallback.invoke(message!!)

    }
}