package com.thong.chatmvp.repositories.remote.response

import android.graphics.Picture
import com.google.gson.annotations.SerializedName

data class UserRespone (
    @SerializedName("data")
    val `data`: List<DataUser?>?,
    @SerializedName("error_code")
    val errorCode: Int?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("sub_code")
    val subCode: Int?,
    @SerializedName("title")
    val title: String?
)

data class DataUser(
    @SerializedName("group_name")
    val groupName: String?,
    @SerializedName("picture")
    val picture: Picture?,
    @SerializedName("user_list")
    val userList: List<User?>?,
    @SerializedName("user_type")
    val userType: Int?
)
data class User(
    @SerializedName("content_id")
    val contentId: Int?,
    @SerializedName("content_path")
    val contentPath: String?,
    @SerializedName("first_name")
    val firstName: String?,
    @SerializedName("full_name")
    val fullName: String?,
    @SerializedName("last_name")
    val lastName: String?,
    @SerializedName("object_user_id")
    val objectUserId: String?,
    @SerializedName("online")
    var online: Boolean?,
    @SerializedName("user_id")
    var userId: Int?,
    @SerializedName("user_type")
    val userType: Int?,
    @SerializedName("user_type_name")
    val userTypeName: String?
)

data class Picture(
    @SerializedName("content_id")
    val contentId: Int?,
    @SerializedName("content_path")
    val contentPath: String?
)