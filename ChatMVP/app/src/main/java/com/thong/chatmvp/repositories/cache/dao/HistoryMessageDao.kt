package com.thong.chatmvp.repositories.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.thong.chatmvp.repositories.cache.entity.MessageEntity
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface HistoryMessageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllMsg(msgData: List<MessageEntity>) : Completable

    @Insert
    fun insertMsg(msgData: MessageEntity) : Completable

    @Query("SELECT * FROM message_msg")
    fun getAllMsg() : Observable<List<MessageEntity>>

    @Query("SELECT * FROM message_msg WHERE chat_room_id = :roomId")
    fun getMsgByRoom(roomId: Int) : Observable<List<MessageEntity>>

    @Query("SELECT COUNT(user_id) FROM message_msg WHERE user_id = :user_id")
    fun getMyMsgCount(user_id:Int) : Observable<Int>

    @Query("UPDATE message_msg SET send_msg_status = :send_msg_status WHERE send_msg_status = 0 OR send_msg_status = 1")
    fun updateMsgStatus(send_msg_status: Int) : Completable

    @Query("UPDATE message_msg SET chat_msg_id = :chatMsgId,chat_msg_type_id = :chatMsgTypeId,send_msg_status = :sendMsgStatus, message_time = :timeStamp WHERE local_msg_id = :localMsgId AND user_id = :userId")
    fun updateMsgStatusAndTimeStamp(chatMsgId: Int, chatMsgTypeId: Int, sendMsgStatus: Int, timeStamp: String, localMsgId: String, userId: Int) : Completable
}