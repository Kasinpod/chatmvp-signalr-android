package com.thong.chatmvp.view.message

import android.annotation.SuppressLint
import android.util.Log
import com.thong.chatmvp.constant.Config
import com.thong.chatmvp.repositories.cache.AppDatabase
import com.thong.chatmvp.repositories.cache.entity.MessageEntity
import com.thong.chatmvp.repositories.remote.CallApi
import com.thong.chatmvp.repositories.remote.response.MessageHistoryRespone
import com.thong.chatmvp.model.DataChat
import com.thong.chatmvp.utility.MySharedPreference
import com.thong.chatmvp.utility.RetrofitBuilder
import io.reactivex.CompletableObserver
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MessagePresenter(private val view: MessageView, private val appDatabase: AppDatabase) {

    var retrofit = RetrofitBuilder().build<CallApi>(Config.configUrl.BASE_URL)

    fun getHistoryMessage(roomId: Int, currentPage: Int) {
        retrofit.getMessageHistory(
            "Bearer ${MySharedPreference.getToken()!!}",
            0,
            roomId,
            MySharedPreference.getUserID(),
            1,
            50,
            currentPage
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<MessageHistoryRespone> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(t: MessageHistoryRespone) {
                    view.responseHistory(t)
                }

                override fun onError(e: Throwable) {

                }

            })
    }

    fun insertAllMsg(msgData: List<MessageEntity>,roomId: Int) {
        Log.e("userData", "" + msgData)
        appDatabase.HistoryMessageDao().insertAllMsg(msgData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onComplete() {
                    loadDataMsg(roomId)
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {
                    Log.d("RxJava", e.message.toString())
//                    mainView.addMsgFail(e.message.toString())
                }
            })
    }

    fun insertMsg(msgData: MessageEntity) {
        appDatabase.HistoryMessageDao().insertMsg(msgData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onComplete() {
                    loadDataMsg(msgData.chatRoomId!!)
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {
                    Log.d("RxJava", e.message.toString())
                }
            })
    }

    fun updateStatusSendMsgAndTimeStamp(data: DataChat) {
        appDatabase.HistoryMessageDao()
            .updateMsgStatusAndTimeStamp(
                data.chatMsgId,
                data.chatMsgTypeId,
                2,
                data.dateTime,
                data.localMsgId,
                data.userId
            )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : CompletableObserver {
                override fun onComplete() {
                    loadDataMsg(data.chatRoomId)
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onError(e: Throwable) {
                }
            })
    }

    @SuppressLint("CheckResult")
    private fun loadDataMsg(roomId: Int) {
        appDatabase.HistoryMessageDao().getMsgByRoom(roomId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { msg -> view.updateUI(msg) },
                { error -> Log.d("RxJava", "Error getting info from interactor into presenter") }
            )
    }
}
