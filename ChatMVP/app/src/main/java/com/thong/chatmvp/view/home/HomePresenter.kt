package com.thong.chatmvp.view.home

import com.thong.chatmvp.constant.Config
import com.thong.chatmvp.repositories.remote.CallApi
import com.thong.chatmvp.repositories.remote.request.GetRoomRequest
import com.thong.chatmvp.repositories.remote.response.GetRoomResponse
import com.thong.chatmvp.repositories.remote.response.User
import com.thong.chatmvp.repositories.remote.response.UserRespone
import com.thong.chatmvp.model.SectionItem
import com.thong.chatmvp.utility.MySharedPreference
import com.thong.chatmvp.utility.RetrofitBuilder
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject

class HomePresenter(private val view: HomeView) {

    var retrofit = RetrofitBuilder().build<CallApi>(Config.configUrl.BASE_URL)

    fun getUser() {
        retrofit.getUserList("Bearer ${MySharedPreference.getToken()!!}", 1, 1)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<UserRespone> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(t: UserRespone) {
                    view.updateUI(t)
                }

                override fun onError(e: Throwable) {

                }

            })
    }

    fun getRoom(model: GetRoomRequest) {
        println("Bearer ${MySharedPreference.getToken()!!}")

        println("ObjectID ${MySharedPreference.getObjectID()!!}")
        println("UserID ${MySharedPreference.getUserID()!!}")
        println("WSPID ${MySharedPreference.getWSPID()!!}")

        println("list model $model")

        retrofit.getRoom(
            "Bearer ${MySharedPreference.getToken()!!}",
            MySharedPreference.getObjectID().toString(),
            MySharedPreference.getUserID(),
            MySharedPreference.getWSPID(),
            model
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<GetRoomResponse> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(t: GetRoomResponse) {
                    view.roomResponse(t)
                }

                override fun onError(e: Throwable) {
                }

            })
    }

    fun setDate(model: UserRespone): ArrayList<SectionItem> {

        val group: ArrayList<SectionItem> = ArrayList()

        model.data!!.forEach {
            val section = SectionItem()
            val item: ArrayList<User> = ArrayList()
            section.title = it?.groupName.toString()
            it?.userList?.forEach {
                item.add(it!!)
            }
            section.items = item
            group.add(section)
        }
        return group

    }

    fun mockUpData(): ArrayList<SectionItem> {
        val group: ArrayList<SectionItem> = ArrayList()
        val section = SectionItem()
        section.title = ""
        section.items = ArrayList()
        group.add(section)
        return group

    }
}
