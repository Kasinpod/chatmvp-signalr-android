package com.thong.chatmvp.utility

import java.util.*
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec

object CryptoUtility {

    private val HMAC_SHA384 = "HmacSHA384"

    private fun toHexString(bytes: ByteArray): String {
        val formatter = Formatter()
        for (b in bytes) {
            formatter.format("%02x", b)
        }
        return formatter.toString()
    }

    fun toHMACSHA384(key: String, data: String): String {
        val secretKeySpec = SecretKeySpec(key.toByteArray(), HMAC_SHA384)
        val mac: Mac = Mac.getInstance(HMAC_SHA384)
        mac.init(secretKeySpec)
        return toHexString(mac.doFinal(data.toByteArray()))
    }
}