package com.thong.chatmvp.repositories.cache.entity

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "message_msg")
data class MessageEntity(

    @PrimaryKey
    @ColumnInfo(name = "local_msg_id") @NonNull
    val localMsgId: String,
    @ColumnInfo(name = "chat_msg_id")
    val chatMsgId: Int?,
    @ColumnInfo(name = "chat_msg_type_id")
    val chatMsgTypeId: Int?,
    @ColumnInfo(name = "chat_room_id")
    val chatRoomId: Int?,
    @ColumnInfo(name = "text")
    val text: String?,
    @ColumnInfo(name = "user_id")
    val userId: Int?,
    @ColumnInfo(name = "send_msg_status")
    var send_msg_status: Int, // 0 unsend 2 sended
    @ColumnInfo(name = "message_time")
    var massage_time: String
)