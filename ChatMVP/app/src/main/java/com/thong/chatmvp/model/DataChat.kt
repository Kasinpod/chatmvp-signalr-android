package com.thong.chatmvp.model


import com.google.gson.annotations.SerializedName

data class DataChat(
    @SerializedName("chat_msg_id")
    val chatMsgId: Int,
    @SerializedName("chat_msg_type_id")
    val chatMsgTypeId: Int,
    @SerializedName("chat_room_id")
    val chatRoomId: Int,
    @SerializedName("dateTime")
    val dateTime: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("local_msg_id")
    val localMsgId:String,
    @SerializedName("user_id")
    val userId: Int,
    @SerializedName("noti_number")
    val notiNumber: Long
)