package com.thong.chatmvp.view.message.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.thong.chatmvp.R
import com.thong.chatmvp.constant.MsgType
import com.thong.chatmvp.repositories.cache.entity.MessageEntity
import com.thong.chatmvp.utility.MySharedPreference

class ChatAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var itemList: List<MessageEntity> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view: View
        val holder: RecyclerView.ViewHolder

        when (viewType) {
            MsgType.MYCHAT -> {
                view = inflater.inflate(R.layout.my_msg, parent, false)
                holder = MyChatViewHolder(view)
            }
            MsgType.OTHERCHAT -> {
                view = inflater.inflate(R.layout.other_msg, parent, false)
                holder = OtherChatViewHolder(view)
            }
            else -> {
                view = inflater.inflate(R.layout.other_msg, parent, false)
                holder = OtherChatViewHolder(view)
            }
        }
        return holder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == MsgType.MYCHAT)
            myChatView(holder as MyChatViewHolder,position)
        else if (holder.itemViewType == MsgType.OTHERCHAT)
            otherChatView(holder as OtherChatViewHolder,position)
    }

    private fun myChatView(holder: MyChatViewHolder, position: Int) {
        val item = itemList[position]
        holder.myMsgText.text = item.text

        if (item.send_msg_status != 2) {
            holder.buttonRetry.visibility = View.VISIBLE
        }else{
            holder.buttonRetry.visibility = View.GONE
        }
    }

    inner class MyChatViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        internal var myMsgText = itemView.findViewById<TextView>(R.id.textViewMyMsg)
        internal var buttonRetry = itemView.findViewById<ImageView>(R.id.buttonRetry)

    }

    private fun otherChatView(holder: OtherChatViewHolder, position: Int) {
        val item = itemList[position]
        holder.myOtherMsgText.text = item.text
    }

    inner class OtherChatViewHolder internal constructor(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        internal var myOtherMsgText = itemView.findViewById<TextView>(R.id.textViewOtherMsg)
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int {
        return when (itemList[position].userId) {
            MySharedPreference.getUserID() -> MsgType.MYCHAT
            else -> MsgType.OTHERCHAT
        }
    }
}

