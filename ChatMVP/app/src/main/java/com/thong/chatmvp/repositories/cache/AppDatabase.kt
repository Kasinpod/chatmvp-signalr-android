package com.thong.chatmvp.repositories.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import com.thong.chatmvp.repositories.cache.dao.HistoryMessageDao
import com.thong.chatmvp.repositories.cache.entity.MessageEntity

@Database(
    entities = [MessageEntity::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun HistoryMessageDao(): HistoryMessageDao
}